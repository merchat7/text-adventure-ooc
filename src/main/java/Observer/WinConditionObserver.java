public class WinConditionObserver implements Observer {

    private Session session = Global.SESSION;

    @Override
    public void execute() {
        int currentLevel = session.getCurrentLevel();
        if (levelFinished(currentLevel)) {
            if (currentLevel == Game.FINAL_LEVEL) {
                System.out.println("You've finished all levels, you win!");
                System.exit(0);
            }
            session.setCurrentLevel(currentLevel += 1);
            System.out.println(String.format("Congratulation! You are now on level %d (All HP recovered)", currentLevel));
            LevelFactory.initLevel(session);
        }
    }

    private boolean levelFinished(int currentLevel) {
        GameStats gameStats = session.getGameStats();
        boolean finished = false;
        int conditionCount = 0;

        for (Class<? extends Condition> winCondition : gameStats.getWinConditions()) {
            try {
                Condition testCondition = winCondition.getConstructor(Session.class).newInstance(session);
                conditionCount++;
                if (gameStats.isMustSatisfyAllWinConditions()) {
                    if (conditionCount == 1) {
                        finished = testCondition.isSatisfied();
                        continue; // in case testing the condition is expensive
                    }
                    if (finished)
                        finished = testCondition.isSatisfied(); // will remain true as long as all testCondition == true
                    else break;
                } else if (testCondition.isSatisfied()) return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return finished;
    }
}
