public interface Observer {
    void execute();
}
