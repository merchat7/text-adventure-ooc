import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LoseConditionObserver implements Observer {
    private Session session = Global.SESSION;

    @Override
    public void execute() {
        List<Condition> loseConditions = new ArrayList<Condition>() {{
            // Add new lose conditions here
            add(new LoseConditionZeroPlayerHP(session));
        }};
        // If any of the lose conditions is true, end the game
        // *Does not support if multiple lose conditions must be true simultaneously*
        for (Condition loseCondition : loseConditions) {
            if (loseCondition.isSatisfied()) {
                retryPrompt();
                break;
            }
        }
    }

    private void retryPrompt() {
        Scanner scanner = new Scanner(System.in);
        String inputCommand = "";
        System.out.println("Retry? y/n");
        while (!inputCommand.equals("n")) {
            String[] line = scanner.nextLine().split(" ");
            inputCommand = line[0].toLowerCase();
            if (inputCommand.equals("y")) {
                Session newSession = new Session();
                Game.newGame(newSession);
                session.setSession(newSession);
                return;
            } else if (!inputCommand.equals("n")) System.out.println("Please write y/n");
        }
        gameOver();
    }

    private void gameOver() {
        System.out.println("Game over");
        System.out.println("Exiting...");
        System.exit(0);
    }
}
