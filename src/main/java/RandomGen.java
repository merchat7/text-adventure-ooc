import java.util.concurrent.ThreadLocalRandom;

public class RandomGen {
    public static int generateValue(int value, double seed, double variance) {
        int min = (int) Math.round(value * seed * (1 - variance));
        int max = (int) Math.round(value * seed * (1 + variance));
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static int generateValue(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
