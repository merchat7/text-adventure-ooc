public class WinConditionKeyFound implements Condition {
    private Player player;

    public WinConditionKeyFound(Session session) {
        this.player = session.getPlayer();
    }

    @Override
    public boolean isSatisfied() {
        return player.isHasKey();
    }
}
