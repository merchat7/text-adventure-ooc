public class WinConditionNoEnemyRemaining implements Condition {
    private int enemyRemaining;

    public WinConditionNoEnemyRemaining(Session session) {
        this.enemyRemaining = session.getGameStats().getEnemyRemaining();
    }

    @Override
    public boolean isSatisfied() {
        return enemyRemaining == 0;
    }
}
