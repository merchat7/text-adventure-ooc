public class WinConditionBossKilled implements Condition {
    private Session session;

    public WinConditionBossKilled(Session session) {
        this.session = session;
    }

    @Override
    public boolean isSatisfied() {
        return session.getGameStats().isBossKilled();
    }
}
