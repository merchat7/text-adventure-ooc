public interface Condition {
    boolean isSatisfied();
}
