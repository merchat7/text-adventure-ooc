public class LoseConditionZeroPlayerHP implements Condition {
    private Player player;

    public LoseConditionZeroPlayerHP(Session session) {
        this.player = session.getPlayer();
    }

    @Override
    public boolean isSatisfied() {
        if (player.getHp() <= 0) {
            System.out.println("You've died");
            return true;
        }
        return false;
    }
}