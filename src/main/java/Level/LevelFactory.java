import java.util.ArrayList;
import java.util.List;

public class LevelFactory {
    private static List<Class<? extends Level>> levels = new ArrayList<>();

    // Add new levels here
    static {
        // ORDER MATTERS e.g. add level1 first, then level2, etc.
        levels.add(Level1.class);
        levels.add(Level2.class);
        levels.add(Level3.class);
    }

    public static void initLevel(Session session) {
        int currentLevel = session.getCurrentLevel();
        Level stage = LevelFactory.createLevel(currentLevel);
        session.setSession(stage);
        session.getGameStats().addObserver(new WinConditionObserver());
    }

    private static Level createLevel(int level) {
        if (levels.size() >= level) {
            try {
                return levels.get(level - 1).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
