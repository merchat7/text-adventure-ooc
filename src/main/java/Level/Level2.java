import java.util.Arrays;

public class Level2 extends Level {
    public Level2() {
        Room[][] map = new Room[2][2];
        map[0][0] = RoomFactory.createRoom(Arrays.asList("east", "south"), 1.3); // A
        map[0][1] = RoomFactory.createRoom(Arrays.asList("west", "south"), 1.4); // B
        map[1][0] = RoomFactory.createRoom(Arrays.asList("north"), 1.2); // C
        map[1][1] = RoomFactory.createRoom(Arrays.asList("north"), 1.5); // D
        //   0   1
        // 0 A - B
        //   |   |
        // 1 C   D
        Room starter = map[1][0];
        RoomFactory.spawnRoom(starter);

        setMap(map);
        setSpawnKey(true);
        setStartLoc(Arrays.asList(1, 0));
        setWinInstruction("Find the key to win [Hint: Killing monster may drop a key]");

        GameStats gameStats = new GameStats();
        gameStats.addWinCondition(WinConditionKeyFound.class);
        setGameStats(gameStats);
    }

}
