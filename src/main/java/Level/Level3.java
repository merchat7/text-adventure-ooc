import java.util.Arrays;

public class Level3 extends Level {
    public Level3() {
        Room[][] map = new Room[2][3];
        map[0][0] = RoomFactory.createRoom(Arrays.asList("east"), 1.7); // A
        map[0][1] = RoomFactory.createRoom(Arrays.asList("east", "west", "south"), 1.6); // B
        map[0][2] = RoomFactory.createRoom(Arrays.asList("west", "exit"), 1.7); // C
        map[1][1] = RoomFactory.createRoom(Arrays.asList("north"), 1.5); // D
        map[1][2] = RoomFactory.createRoom(Arrays.asList("north"), 2); // E (special [boss] room)
        //   0   1   2
        // 0 A - B - C
        //       |   |
        // 1     D   E
        Room starter = map[1][1];
        RoomFactory.spawnRoom(starter);

        setMap(map);
        setSpawnKey(false);
        setStartLoc(Arrays.asList(1, 1));
        setWinInstruction("Find the exit to win");

        GameStats gameStats = new GameStats();
        gameStats.addWinCondition(WinConditionBossKilled.class);
        setGameStats(gameStats);
    }
}