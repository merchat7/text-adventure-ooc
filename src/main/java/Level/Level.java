import java.util.List;

public abstract class Level {
    private Room[][] map;
    private boolean spawnKey;
    private List<Integer> startLoc;
    private String winInstruction;
    private GameStats gameStats;

    public Room[][] getMap() {
        return map;
    }

    public void setMap(Room[][] map) {
        this.map = map;
    }

    public boolean isSpawnKey() {
        return spawnKey;
    }

    public void setSpawnKey(boolean spawnKey) {
        this.spawnKey = spawnKey;
    }

    public List<Integer> getStartLoc() {
        return startLoc;
    }

    public void setStartLoc(List<Integer> startLoc) {
        this.startLoc = startLoc;
    }

    public String getWinInstruction() {
        return winInstruction;
    }

    public void setWinInstruction(String winInstruction) {
        this.winInstruction = winInstruction;
    }

    public GameStats getGameStats() {
        return gameStats;
    }

    public void setGameStats(GameStats gameStats) {
        this.gameStats = gameStats;
    }
}
