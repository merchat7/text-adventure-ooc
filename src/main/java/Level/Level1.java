import java.util.Arrays;

public class Level1 extends Level {
    public Level1() {
        Room[][] map = new Room[1][3];
        map[0][0] = RoomFactory.createRoom(Arrays.asList("east"), 1); // A
        map[0][1] = RoomFactory.createRoom(Arrays.asList("east", "west"), 1.1); // B
        map[0][2] = RoomFactory.createRoom(Arrays.asList("west"), 1.2); // C
        //   0   1   2
        // 0 A - B - C
        Room starter = map[0][0];
        RoomFactory.spawnRoom(starter);

        setMap(map);
        setSpawnKey(false);
        setStartLoc(Arrays.asList(0, 0));
        setWinInstruction("Kill 3 enemy to win");

        GameStats gameStats = new GameStats();
        gameStats.addWinCondition(WinConditionNoEnemyRemaining.class);
        gameStats.setEnemyRemaining(3);
        setGameStats(gameStats);
    }
}
