import java.util.concurrent.ThreadLocalRandom;

public class Drop {
    public static boolean keyDropped() {
        if (ThreadLocalRandom.current().nextDouble() <= Difficulty.KEY_SPAWN_PROB) {
            System.out.println("Found the key!");
            return true;
        }
        return false;
    }
}
