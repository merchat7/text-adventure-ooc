public class Weapon {
    private int ap = Difficulty.WEAPON_AP_DEFAULT;
    private int hit = Difficulty.WEAPON_HIT_DEFAULT;

    public int getAp() {
        return ap;
    }

    public void setAp(int ap) {
        this.ap = ap;
    }

    public int getHit() {
        return hit;
    }

    public void setHit(int hit) {
        this.hit = hit;
    }
}
