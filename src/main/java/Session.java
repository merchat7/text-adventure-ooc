public class Session {
    private Room[][] map;
    private Player player = new Player(Difficulty.PLAYER_HP, Difficulty.PLAYER_AP);
    private int currentLevel = 1;
    private boolean spawnKey;
    private GameStats gameStats;

    public void setSession(Level level) {
        map = level.getMap();
        player.setCurrentLoc(level.getStartLoc());
        spawnKey = level.isSpawnKey();
        gameStats = level.getGameStats();
        System.out.println(level.getWinInstruction());
        // Keep weapon from previous level
        // Keep AP gained from previous level
        // Recover all player's hp
        player.setHp(player.getMaxHp());
        player.setHasKey(false);
    }

    public void setSession(Session session) {
        map = session.getMap();
        player = session.getPlayer();
        currentLevel = session.getCurrentLevel();
        spawnKey = session.isSpawnKey();
        gameStats = session.getGameStats();
    }

    public Room[][] getMap() {
        return map;
    }

    public Player getPlayer() {
        return player;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public boolean isSpawnKey() {
        return spawnKey;
    }

    public GameStats getGameStats() {
        return gameStats;
    }


}
