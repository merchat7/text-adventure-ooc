public class Parser {
    public static int getWeaponSlot(String command) {
        int lengthWordWeapon = "weapon".length();
        int slot;
        if (command.length() <= lengthWordWeapon || !command.substring(0, lengthWordWeapon).equals("weapon"))
            System.out.println("Invalid syntax: USAGE: weapon[slot] e.g. weapon2");
        else {
            try {
                slot = Integer.parseInt(command.substring(lengthWordWeapon));
                if (slot <= 0 || slot > 2)
                    System.out.println("Invalid slot specified"); // 2 = max inventory size (hard coded)
                else return slot;
            } catch (NumberFormatException e) {
                System.out.println("Invalid syntax: USAGE: weapon[slot] e.g. weapon2");
            }
        }
        return -1;
    }
}
