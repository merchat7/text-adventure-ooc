import java.util.concurrent.ThreadLocalRandom;

public class WeaponFactory {

    public static Weapon createDefaultWeapon() {
        return new Weapon();
    }

    public static Weapon spawnRandomWeapon(double seed, double prob) {
        Weapon weapon = null;
        if (ThreadLocalRandom.current().nextDouble() <= prob) {
            weapon = new Weapon();
            weapon.setAp(RandomGen.generateValue(Difficulty.WEAPON_AP, seed, 0.5));
            weapon.setHit(RandomGen.generateValue(Difficulty.WEAPON_MIN_HIT, Difficulty.WEAPON_MAX_HIT));
        }
        return weapon;
    }
}
