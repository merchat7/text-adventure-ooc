import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class Player extends Character implements Observable {
    private boolean hasKey = false;
    private Weapon equippedWeapon;
    private List<Weapon> inventory = new ArrayList<Weapon>() {{
        add(WeaponFactory.createDefaultWeapon());
    }};
    private List<Integer> currentLoc;
    private List<Observer> observers = new ArrayList<>();

    public Player(int hp, int ap) {
        this.setHp(hp);
        this.setMaxHp(hp);
        this.setAp(ap);
    }

    @Override
    public void setHp(int hp) {
        super.setHp(hp);
        if (hp <= 0) notifyObservers();
    }

    public boolean isHasKey() {
        return hasKey;
    }

    public void setHasKey(boolean hasKey) {
        this.hasKey = hasKey;
        if (hasKey) notifyObservers();
    }

    public Weapon getEquippedWeapon() {
        return equippedWeapon;
    }

    public void setEquippedWeapon(Weapon equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }

    public List<Weapon> getInventory() {
        return inventory;
    }

    public List<Integer> getCurrentLoc() {
        return currentLoc;
    }

    public void setCurrentLoc(List<Integer> currentLoc) {
        this.currentLoc = currentLoc;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers() {
        try {
            for (Observer observer : observers) {
                observer.execute();
            }
        } catch (ConcurrentModificationException e) {
        } // will trigger due to making a new game if player retry after they lose (not critical so just ignore)
    }
}
