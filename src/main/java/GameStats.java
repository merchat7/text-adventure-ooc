import java.util.ArrayList;
import java.util.List;

public class GameStats implements Observable {
    private int enemyRemaining = -1;
    private boolean bossKilled = false;
    private boolean mustSatisfyAllWinConditions = false;
    private List<Class<? extends Condition>> winConditions = new ArrayList<>();
    private List<Observer> observers = new ArrayList<>();

    public int getEnemyRemaining() {
        return enemyRemaining;
    }

    public void setEnemyRemaining(int enemyRemaining) {
        this.enemyRemaining = enemyRemaining;
        if (enemyRemaining == 0) notifyObservers();
    }

    public boolean isBossKilled() {
        return bossKilled;
    }

    public void setBossKilled(boolean bossKilled) {
        this.bossKilled = bossKilled;
        if (bossKilled) notifyObservers();
    }

    public boolean isMustSatisfyAllWinConditions() {
        return mustSatisfyAllWinConditions;
    }

    public void setMustSatisfyAllWinConditions(boolean bool) {
        this.mustSatisfyAllWinConditions = bool;
    }

    public void addWinCondition(Class<? extends Condition> condition) {
        winConditions.add(condition);
    }

    public List<Class<? extends Condition>> getWinConditions() {
        return winConditions;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.execute();
        }
    }
}
