import java.util.List;

public class Room {
    private List<String> doors;
    private double seed;
    private Enemy enemy = null;
    private Weapon weapon = null;

    public static Room locToRoom(List<Integer> loc, Room[][] map) {
        int y = loc.get(0), x = loc.get(1);
        return map[y][x];
    }

    public List<String> getDoors() {
        return doors;
    }

    public void setDoors(List<String> doors) {
        this.doors = doors;
    }

    public double getSeed() {
        return seed;
    }

    public void setSeed(double seed) {
        this.seed = seed;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
