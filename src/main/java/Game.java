import java.util.List;
import java.util.Scanner;

public class Game {
    public final static int FINAL_LEVEL = 3;
    public final static int INVENTORY_MAX_SIZE = 2;
    //private Session session = new Session();
    private Session session = Global.SESSION;

    public void playGame() {
        System.out.println("Game starting...");
        newGame(session);
        Scanner scanner = new Scanner(System.in);
        String inputCommand = "";
        while (!inputCommand.equals("quit")) {
            List<Integer> currentLoc = session.getPlayer().getCurrentLoc();
            int y = currentLoc.get(0), x = currentLoc.get(1);
            System.out.println(String.format(">> [Current location is: (%d,%d)]", y, x));
            String[] line = scanner.nextLine().split(" ");
            inputCommand = line[0];
            /*
            if (inputCommand.equals("die")) {
                session.getPlayer().setHp(0);
            } else if (inputCommand.equals("power")) {
                session.getPlayer().setAp(10000);
                session.getPlayer().getInventory().get(0).setHit(100);
            }
            */
            CommandFactory.runCommand(session, line);
        }
        System.out.println("Exiting the game..."); // the "quit" command
    }

    public static void newGame(Session session) {
        LevelFactory.initLevel(session);
        Player player = session.getPlayer();
        player.addObserver(new WinConditionObserver());
        player.addObserver(new LoseConditionObserver());
        System.out.println("Beginning level 1");
    }
}
