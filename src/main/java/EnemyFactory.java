import java.util.concurrent.ThreadLocalRandom;

public class EnemyFactory {
    public static Enemy spawnEnemy(double seed, double prob) {
        Enemy enemy = null;
        if (ThreadLocalRandom.current().nextDouble() <= prob) {
            enemy = new Enemy();
            double variance = 0.2;
            enemy.setHp(RandomGen.generateValue(Difficulty.MONSTER_HP, seed, variance));
            enemy.setMaxHp(enemy.getHp());
            enemy.setAp(RandomGen.generateValue(Difficulty.MONSTER_AP, seed, variance));
            enemy.setHit(RandomGen.generateValue(Difficulty.MONSTER_MIN_HIT, Difficulty.MONSTER_MAX_HIT));
        }
        return enemy;
    }
}
