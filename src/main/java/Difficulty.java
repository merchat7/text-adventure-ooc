public class Difficulty {
    public final static int PLAYER_HP = 20;
    public final static int PLAYER_AP = 5;
    public final static int WEAPON_AP = 5;
    public final static int WEAPON_MIN_HIT = 60;
    public final static int WEAPON_MAX_HIT = 100;
    public final static int WEAPON_AP_DEFAULT = 4;
    public final static int WEAPON_HIT_DEFAULT = 70;
    public final static int MONSTER_HP = 20;
    public final static int MONSTER_AP = 5;
    public final static int MONSTER_MIN_HIT = 25;
    public final static int MONSTER_MAX_HIT = 75;
    public final static double WEAPON_SPAWN_PROB = 0.4;
    public final static double KEY_SPAWN_PROB = 0.4;
    public final static double MONSTER_SPAWN_PROB = 0.3;
}
