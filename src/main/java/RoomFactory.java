import java.util.List;

public class RoomFactory {

    public static Room createRoom(List<String> doors, double seed) {
        Room room = new Room();
        room.setDoors(doors);
        room.setSeed(seed);
        return room;
    }

    public static void spawnRoom(Room room) {
        double seed = room.getSeed();
        // Only spawn things if nothing is there
        if (room.getEnemy() == null) {
            Enemy enemy = EnemyFactory.spawnEnemy(seed, Difficulty.MONSTER_SPAWN_PROB);
            room.setEnemy(enemy);
        }
        if (room.getWeapon() == null) {
            Weapon weapon = WeaponFactory.spawnRandomWeapon(seed, Difficulty.WEAPON_SPAWN_PROB);
            room.setWeapon(weapon);
        }
    }
}
