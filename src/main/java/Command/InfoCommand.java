import java.util.List;

public class InfoCommand extends Command {
    private Player player;
    private Room room;
    private List<Weapon> inventory;

    public InfoCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        player = session.getPlayer();
        room = Room.locToRoom(player.getCurrentLoc(), session.getMap());
        inventory = player.getInventory();
    }

    @Override
    public void execute() {
        printInfo();
    }

    private void printInfo() {
        System.out.println("----Player info----");
        System.out.println(String.format("Player HP = %d/%d, Player AP = %d", player.getHp(), player.getMaxHp(), player.getAp()));
        // Max inventory space = 2
        for (int i = 1; i <= Game.INVENTORY_MAX_SIZE; i++) {
            if (i <= inventory.size()) {
                Weapon weaponInventory = inventory.get(i - 1);
                System.out.println(String.format("Weapon%d: AP = %d, Hit = %d%%", i, weaponInventory.getAp(), weaponInventory.getHit()));
            } else {
                System.out.println(String.format("Slot%d is empty", i));
            }
        }
        System.out.println("----Room info----");
        List<String> doors = room.getDoors();
        System.out.println("Doors at: " + doors);
        Enemy enemy = room.getEnemy();
        Weapon weaponRoom = room.getWeapon();
        if (enemy == null) System.out.println("There is no enemy");
        else
            System.out.println(String.format("Enemy HP = %d/%d, Enemy AP = %d, Enemy Hit = %d%%", enemy.getHp(), enemy.getMaxHp(), enemy.getAp(), enemy.getHit()));
        if (weaponRoom == null) System.out.println("There is no item");
        else
            System.out.println(String.format("Weapon AP = %d, Weapon Hit = %d%%", weaponRoom.getAp(), weaponRoom.getHit()));
        System.out.println("-------------------");
    }
}
