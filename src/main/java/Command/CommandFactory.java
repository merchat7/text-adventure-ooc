import java.util.HashMap;

public class CommandFactory {
    private static final HashMap<String, Class<? extends Command>> commands = new HashMap<String, Class<? extends Command>>() {
        {
            put("attack", AttackCommand.class);
            put("drop", DropCommand.class);
            put("go", GoCommand.class);
            put("help", HelpCommand.class);
            put("info", InfoCommand.class);
            put("rest", RestCommand.class);
            put("take", TakeCommand.class);
            put("whistle", WhistleCommand.class);
        }
    };

    public static void runCommand(Session session, String[] inputLine) {
        String name = inputLine[0].toLowerCase();
        Class<? extends Command> commandClass = commands.get(name);
        Command command;
        if (commandClass == null) {
            System.out.println("Invalid command");
        } else {
            try {
                command = commandClass.getConstructor(Session.class, String[].class).newInstance(session, inputLine);
                command.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
