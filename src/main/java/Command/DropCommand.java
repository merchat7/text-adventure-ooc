import java.util.List;

public class DropCommand extends Command {
    private List<Weapon> inventory;

    public DropCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        inventory = session.getPlayer().getInventory();
    }

    @Override
    public void execute() {
        if (inventory.size() == 1) {
            System.out.println("You must be carrying at least one weapon");
            return;
        }
        String weaponCommand = inputLine[1].toLowerCase();
        int slot = Parser.getWeaponSlot(weaponCommand);
        if (slot != -1) inventory.remove(slot - 1);
    }
}
