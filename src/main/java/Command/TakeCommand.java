import java.util.List;

public class TakeCommand extends Command {
    private Room room;
    private List<Weapon> inventory;

    public TakeCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        Player player = session.getPlayer();
        room = Room.locToRoom(player.getCurrentLoc(), session.getMap());
        inventory = player.getInventory();
    }

    @Override
    public void execute() {
        Weapon weapon = room.getWeapon();
        if (weapon == null) {
            System.out.println("No weapon to pick up");
            return;
        }
        if (inventory.size() >= Game.INVENTORY_MAX_SIZE) {
            System.out.println("Inventory is full");
            return;
        }
        inventory.add(weapon);
        room.setWeapon(null);
    }
}
