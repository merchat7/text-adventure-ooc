import java.util.Arrays;
import java.util.List;

public class GoCommand extends Command {
    private String direction;
    private Player player;
    private List<Integer> currentLoc;
    private Room[][] map;

    public GoCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        if (inputLine.length > 1) direction = inputLine[1];
        player = session.getPlayer();
        currentLoc = player.getCurrentLoc();
        map = session.getMap();
    }

    @Override
    public void execute() {
        if (inputLine.length == 1) System.out.println("Please specify direction (e.g. north/east/south/west)");
        else {
            List<Integer> newLoc = go();
            if (newLoc == null) return;
            player.setCurrentLoc(newLoc);
            // Increase the player hp upon successful move
            Player player = session.getPlayer();
            int currentHp = player.getHp();
            int hpRecRate = 1;
            int newHp = currentHp + hpRecRate;
            if (newHp <= player.getMaxHp()) player.setHp(newHp);
        }
    }

    private List<Integer> go() {
        // direction is already put to lower case
        int y = currentLoc.get(0), x = currentLoc.get(1);
        Room room = map[y][x];
        List<String> doors = room.getDoors();
        List<Integer> newLoc;
        if (doors.contains(direction)) {
            switch (direction) {
                case "north":
                    newLoc = Arrays.asList(y - 1, x);
                    break;
                case "east":
                    newLoc = Arrays.asList(y, x + 1);
                    break;
                case "south":
                    newLoc = Arrays.asList(y + 1, x);
                    break;
                case "west":
                    newLoc = Arrays.asList(y, x - 1);
                    break;
                case "exit":
                    System.out.println("As you approach the exit, a boss monster appears and blocks your path");
                    newLoc = Arrays.asList(1, 2);  // Hard-coded
                    break;
                default:
                    // should not get here
                    return null;
            }
            populateRoom(Room.locToRoom(newLoc, map), direction, map);
            return newLoc;
        } else {
            System.out.println("Invalid direction");
            return null;
        }
    }

    // Helper to go
    private void populateRoom(Room room, String direction, Room[][] map) {
        if (direction.equals("exit")) {
            // Spawn the boss in the "special" room
            Enemy boss = EnemyFactory.spawnEnemy(room.getSeed(), 1);
            boss.setBoss(true);
            room.setEnemy(boss);
            // No item spawns in the "special" room
        } else RoomFactory.spawnRoom(room);
    }
}
