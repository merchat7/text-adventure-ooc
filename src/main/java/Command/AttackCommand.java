import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class AttackCommand extends Command {
    private Room room;
    private Player player;
    private Enemy enemy;

    public AttackCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        player = session.getPlayer();
        room = Room.locToRoom(player.getCurrentLoc(), session.getMap());
        enemy = room.getEnemy();
    }

    @Override
    public void execute() {
        if (inputLine.length < 3) {
            System.out.println("USAGE: attack with [weapon name]");
            return;
        }
        if (enemy == null) {
            System.out.println("There is no enemy");
            return;
        }
        Weapon weapon = selectWeapon(inputLine[2].toLowerCase());
        if (weapon == null) return;
        player.setEquippedWeapon(weapon);
        attack();
    }

    private Weapon selectWeapon(String command) {
        List<Weapon> inventory = player.getInventory();
        int slot = Parser.getWeaponSlot(command);
        if (slot != -1) return getWeaponFromInventory(inventory, slot);
        return null;
    }

    private void attack() {
        calculateDmg(player, enemy); // Player's attack
        // if monster died, don't take damage from its attack that turn
        if (enemy.getHp() <= 0) {
            boolean isBoss = enemy.isBoss();
            boolean keySpawned = session.isSpawnKey();
            GameStats gameStats = session.getGameStats();
            int enemyRemaining = gameStats.getEnemyRemaining();

            System.out.println("Enemy was defeated, +1 to AP");
            player.setAp(player.getAp() + 1);

            if (keySpawned) player.setHasKey(Drop.keyDropped());
            if (enemyRemaining > 0) gameStats.setEnemyRemaining(enemyRemaining - 1);
            if (isBoss) gameStats.setBossKilled(true);

            room.setEnemy(null);
        } else calculateDmg(enemy, player); // Enemy's attack
    }

    private Weapon getWeaponFromInventory(List<Weapon> inventory, int slot) {
        if (inventory.size() >= slot) return inventory.get(slot - 1);
        else System.out.println(String.format("You don't have a weapon in slot %d", slot));
        return null;
    }

    private void calculateDmg(Character source, Character target) {
        int damage;
        double hit;
        Weapon weapon;
        if (source.getClass().getName().equals("Enemy")) {
            Enemy enemySource = (Enemy) source;
            damage = enemySource.getAp();
            hit = enemySource.getHit() / (double) 100;
        } else {
            Player playerSource = (Player) source;
            weapon = playerSource.getEquippedWeapon();
            damage = playerSource.getAp() + weapon.getAp();
            hit = weapon.getHit() / (double) 100;
        }
        String targetName = target.getClass().getName();
        String sourceName = source.getClass().getName();
        if (ThreadLocalRandom.current().nextDouble() <= hit) {
            int remainingHp = target.getHp() - damage;
            System.out.println(String.format("%s hits %s for %d, (%d %s HP remaining)", sourceName, targetName.toLowerCase(), damage, remainingHp, targetName.toLowerCase()));
            target.setHp(remainingHp);
        } else System.out.println(String.format("%s missed", sourceName));
    }
}
