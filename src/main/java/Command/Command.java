public abstract class Command implements CommandSkeleton {
    Session session;
    String[] inputLine;

    public Command(Session session, String[] inputLine) {
        this.session = session;
        this.inputLine = inputLine;
    }
}
