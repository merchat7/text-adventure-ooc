public class RestCommand extends Command {
    private Room room;
    private Player player;

    public RestCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        player = session.getPlayer();
        room = Room.locToRoom(player.getCurrentLoc(), session.getMap());
    }

    @Override
    public void execute() {
        if (room.getEnemy() == null) {
            player.setHp(player.getMaxHp());
            System.out.println("HP was fully recovered");
        } else System.out.println("Cannot rest while there is an enemy!");
    }
}
