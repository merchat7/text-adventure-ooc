public class WhistleCommand extends Command {
    private Room room;

    public WhistleCommand(Session session, String[] inputLine) {
        super(session, inputLine);
        room = Room.locToRoom(session.getPlayer().getCurrentLoc(), session.getMap());
    }

    @Override
    public void execute() {
        if (room.getEnemy() == null) room.setEnemy(EnemyFactory.spawnEnemy(room.getSeed(), 1));
    }
}
