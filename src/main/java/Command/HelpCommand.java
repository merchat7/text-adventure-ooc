public class HelpCommand extends Command {

    public HelpCommand(Session session, String[] inputLine) {
        super(session, inputLine);
    }

    @Override
    public void execute() {
        printHelp();
    }

    private void printHelp() {
        System.out.println("[Supported commands]");
        System.out.println("info - print the information of the player & room");
        System.out.println("go [direction] - go to the specified [direction]");
        System.out.println("take - pick up the weapon in the room");
        System.out.println("drop [weapon] - drop the specified [weapon]");
        System.out.println("attack with [weapon] - attack enemy with specified [weapon]");
        System.out.println("help - print all supported commands");
        System.out.println("quit - exit the game");
        System.out.println("whistle - spawn a monster in the current room");
        System.out.println("rest - recover all HP when there is no monster");
        System.out.println("----------------");
    }
}
